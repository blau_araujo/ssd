# Shell script descomplicado

> Para informações sobre como adquirir o curso, visite:
> https://blauaraujo.com/cursos/ssd

## Índice das aulas

### Módulo 1: a linguagem

#### [Aula 1: Salve, simpatia](aula01.md)

- [Problema 1](aula01.md#problema-1)
  - [Entendendo o problema](aula01.md#entendendo-o-problema)
- [O que é um shell?](aula01.md#o-que-%C3%A9-um-shell)
- [O que são scripts?](aula01.md#o-que-s%C3%A3o-scripts)
- [O que significa "imprimir"](aula01.md#o-que-significa-imprimir)
  - [O que é um terminal hoje em dia?](aula01.md#o-que-%C3%A9-um-terminal-hoje-em-dia)
- [Como imprimir uma mensagem no terminal?](aula01.md#como-imprimir-uma-mensagem-no-terminal)
  - [Anatomia da linha de um comando](aula01.md#anatomia-da-linha-de-um-comando)
  - [Listagem dos comandos internos do Bash](aula01.md#listagem-dos-comandos-internos-do-bash)
  - [Escrevendo a saída de comandos em arquivos](aula01.md#escrevendo-a-sa%C3%ADda-de-comandos-em-arquivos)
  - [Exportações de variáveis](aula01.md#exporta%C3%A7%C3%B5es-de-vari%C3%A1veis)
- [Finalmente, "Salve, simpatia!"](aula01.md#finalmente-salve-simpatia)
  - [Como criar um script](aula01.md#como-criar-um-script)
  - [A linha do interpretador de comandos (hashbang)](aula01.md#a-linha-do-interpretador-de-comandos-hashbang)
  - [O que significa "executar"](aula01.md#o-que-significa-executar)
  - [O que isso tem a ver com a hashbang?](aula01.md#o-que-isso-tem-a-ver-com-a-hashbang)
  - [Permissão de execução](aula01.md#permiss%C3%A3o-de-execu%C3%A7%C3%A3o)
  - [O que muda com a hashbang?](aula01.md#o-que-muda-com-a-hashbang)
  - [A hashbang é obrigatória?](aula01.md#a-hashbang-%C3%A9-obrigat%C3%B3ria)
  - [O certo não seria usar o ‘env’ na hashbang?](aula01.md#o-certo-n%C3%A3o-seria-usar-o-env-na-hashbang)
- [O script final](aula01.md#o-script-final)

#### [Exercícios da aula 1](aula01-ex.md)

#### Conteúdo dos vídeos da aula 1

- Parte 1: o que é o shell?
  - Introdução: o que vamos descomplicar?
  - Componente da base de sistemas UNIX like
  - O shell quanto às suas funções
  - Por que o sistema precisa de um shell padrão?
  - Modos de operação do shell
- Parte 2: o que é um script?
  - Onde fica o arquivo executável do shell?
  - Dica: toda saída pode ser útil em scripts
  - Introdução às permissões de arquivos
  - Shells padrão
  - Descobrindo a interface CLI
  - Explorando os modos "interativo" e "não interativo"
  - Determinando o modo de operação do shell
  - Na prática, nosso primeiro script
  - A filosofia UNIX
- Parte 3: o que significa "imprimir no terminal"?
  - O que realmente queremos aprender
  - Senta que lá vem história!
  - O terminal como uma abstração
  - Fluxos de dados padrão
  - Terminais emulados
  - Pseudo TTY (PTY)
  - Dica: é possível determinar se estamos no TTY ou no PTY!
  - Listando os fluxos de dados
  - Como fica no console TTY?
  - O que acontece no ambiente gráfico?
  - Demonstrando os novos conceitos
  - Imprimir é o mesmo que escrever no terminal
- Parte 4: como criar e executar scripts?
  - Passos para a criação de scripts
  - Solucionando problemas através da definição de procedimentos
  - Criação de novos arquivos
  - Sobre a permissão de execução
  - O problema 1 já está solucionado!
  - Criando arquivos com redirecionamentos
  - Criando arquivos com o utilitário 'touch'
  - Inserindo comandos em novos arquivos
  - Executando scripts sem permissão de execução
  - Criando o arquivo do script com um editor de textos
  - Dando permissão de execução ao arquivo do script
  - O caminho do arquivo
  - Introdução à hashbang
  - Execução de comandos em arquivos na mesma sessão do shell
  - Como funciona uma hashbang?
  - A hashbang é obrigatória?
  - Portabilidade não se resolve na hashbang!

#### Aula 2: Me chame pelo meu nome (em elaboração)

#### [Exercícios da aula 2](aula02-ex.md)

#### Conteúdo do vídeo da aula 2

- Introdução: um novo problema
- Anatomia de um comando simples
- Um breve comentário sobre comentários
- Demonstrando a construção da linha de um comando
- Exportação apenas para o processo iniciado no comando
- O que são "palavras"
- Utilizando todos os elementos de um comando simples
- Não adianta exportar se o programa não usar!
- Uma primeira solução para o problema
- A estrutura do processo na memória
- Listando o ambiente com o utilitário 'env'
- O que aconteceu com o ambiente na primeira solução do problema
- Exportando variáveis para todos os processos filhos
- Incluindo caminhos na variável 'PATH'
- Removendo o atributo de exportação
- Solucionando o problema com a passagem de argumentos

### Aula 3: Qual é a sua graça? (em elaboração)

#### Exercícios da aula 3 (em elabração)

#### Conteúdo do vídeo da aula 3

- Tudo está nos processos!
- Fluxos de dados também são atributos de processos
- Para que serve aquele quarto descritor de arquivos?
- Listando os descritores de arquivos do processo do shell corrente
- Como ler uma linha de texto na entrada padrão
- A solução para o problema 3: recebendo dados pela entrada padrão

## Apoie o meu trabalho

- PIX: pix@blauaraujo.com
- PayPal: https://bit.ly/paypal-debxp
- Apoio mensal: https://apoia.se/debxpcursos
- Livros: https://uiclap.bio/blau_araujo
- Camisetas: https://reserva.ink/devnull

## Licença

Copyright©2024, [Blau Araujo](mailto:blau@debxp.org) 

[Creative Commons BY-SA 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.pt-br)

