#!/bin/bash

# No mundo real, problemas nunca vêm com enunciados!

# Strings ---------------------------------------------------------------------

version=1.1

author='Blau Araujo'

email='blau@debxp.org'

usage="FizBuz $version

Descrição:

    Para cada número recebido, imprime 'NÚMERO fiz' se for
    divisível por 3, 'NÚMERO buz', se for divisível por 5,
    ou 'NÚMERO fizbuz', se for divisível por 3 e 5. Em
    qualquer outra situação, apenas NÚMERO será impresso.

    Listas de números também podem ser recebidas por STDIN.

Uso:

    fizbuz NÚMEROS
    fizbuz [OPÇÕES [ARGUMENTOS]]

Opções:

    -f ARQUIVO          Recebe lista de números de ARQUIVO.
    -r INÍCIO FIM       Trata INÍCIO e FIM como um intervalo.
    -h                  Exibe esta ajuda.
    -v                  Exibe versão e licença.
"

copyright="FizBuz $version

Copyright (C) 2024, $author <$email>.
Licença GPLv3+: GNU GPL versão 3 ou posterior <https://gnu.org/licenses/gpl.html>
Este é um software livre: você é livre para alterá-lo e redistribuí-lo.
NÃO HÁ QUALQUER GARANTIA, na máxima extensão permitida em lei.

Escrito por $author.
"
error[1]='Arquivo não encontrado!'
error[2]='Argumentos inválidos!'
error[3]='Número inválido!'

# Functions -------------------------------------------------------------------

# Imprime uma mensagem e termina com erro...
die() {
    echo ${error[$1]}
    exit $1
}

# Testa se argumento é um arquivo...
is_file() [[ -f $1 ]]

# Testa se argumento é um inteiro válido...
is_int() [[ $1 =~ ^(0|[1-9][0-9]*)$ ]]

# Lê números passados como argumentos...
read_args() for num; do
    fizbuz $num
done

# Lê lista de números em um arquivo...
read_lines() while read num; do
    read_args $num
done

# Define e avalia uma faixa numérica...
eval_range() {
    is_int $1 && is_int $2 || die 3
    read_args $(eval "echo {$1..$2}")
}

# Processa o fizbuz...
fizbuz() {
    local resultado
    is_int $1 || die 3
    (( $1 % 3 )) || resultado=fiz
    (( $1 % 5 )) || resultado=${resultado}buz
    echo $1 $resultado
}

# Main ------------------------------------------------------------------------

# Recebe dados como argumentos...
if [[ -t 0 ]]; then
    # Se o primeiro argumento for um número válido, todos os demais argumentos
    # serão presumidos como números...
    is_int $1 && { read_args $@; exit; }

    # Caso contrário, os argumentos serão verificados...
    case $1 in
        -f) # Ler os números de um arquivo...
            is_file $2 && read_lines < $2 || die 1
            ;;
        -r) # Lê os números definidos por um intervalo...
            [[ $2 && $3 ]] || die 2
            eval_range $2 $3
            ;;
        -h) # Imprimir a ajuda e termina o script...
            echo "$usage"
            ;;
        -v) # Imprimir a versão e terminar o script...
            echo "$copyright"
            ;;
        *)  # Terminar com erro...
            die 2
    esac

# Recebe dados pela entrada padrão...
else
    read_lines
fi

