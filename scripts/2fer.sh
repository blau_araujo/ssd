#/bin/bash

# -----------------------------------------------------------------------------
# Objetivos...
# -----------------------------------------------------------------------------
#  1. [x] Parâmetros especiais @ e *
#  2. [x] Estrutura de repetição por lista (for)
#  3. [x] Expansões condicionais
#  4. [x] Estruturas de decisão
#  5. [ ] Estruturas de repetição condicionais (while)
#  6. [x] Expressões de teste
#  7. [x] Estados de término
# -----------------------------------------------------------------------------

# Parâmetros do shell (relacionados com argumentos):
# - Parâmetros posicionais (0, 1, 2, 3...);
# - #: número de argumentos
# - @: expande todos os argumentos: $1 $2 $3... Entre aspas: "$1" "$2" "$3"...
# - *: expande todos os argumentos: $1 $2 $3... Entre aspas: "$1c$2c$3..."
# Condição -> Consequência:
# Tomadas de decisão:
# - Estruturas condicionais: if, case e loops condicionais (while e until)
#   while COMANDO (sucesso); do BLOCO DE COMANDOS; done
#   until COMANDO (erro); do BLOCO DE COMANDOS; done
#
# - Operadores condicionais: && e ||
# - Expansões condicionais: expansões condicionais de parâmetros
# Estados de término:
# - Parâmetro especial ?: expande o estado de término do "último" comando executado. 
# Expressões de teste:



# -----------------------------------------------------------------------------

# Problema 4: Escreva um script que atenda às seguintes condições:

# -----------------------------------------------------------------------------
# STDIN está ligada ao terminal
#
#  (se verdadeiro)
#
#  - Há argumentos:
#       se verdadeiro: imprimir para cada argumento: Um para ARGUMENTO, um para mim.
#       se falso     : imprimir apenas: Um para você, um para mim.
# -----------------------------------------------------------------------------

# solução com 'if'...

# if [[ -n "$@"  ]]; then
#     printf 'Um para %s, um para mim.\n' "$@"
# else
#     echo 'Um para você, um para mim.'
# fi

# Solução com encadeamentos condicionais...

# [[ -n "$@"  ]] && printf 'Um para %s, um para mim.\n' "$@" || echo 'Um para você, um para mim.'

# Solução com encadeamentos condicionais decidindo o valor de 'nome'...

# [[ "$@" ]] && nome=$1 || nome=você
# echo "Um para ${1:-você}, um para mim"

# solução com expansão condicional e loop 'for'...

# for var in "${@:-você}"; do
#     echo "Um para $var, um para mim."
# done

# Solução com expansão condicional, mas sem loop...

# printf 'Um para %s, um para mim.\n' "${@:-você}"

# -----------------------------------------------------------------------------
# (Stdin está ligada ao terminal)
# 
# (se falso -> stdin ligada a um arquivo)
#
#  O arquivo tem linhas ou é um pipe:
#       LINHA tem caracteres:
#           - Se verdadeira: imprimir "Um para LINHA, um para mim."
#           - Se falsa: imprimir "Um para você, um para mim."
#       Se falsa, imprimir: "Oba, é tudo meu!"
# -----------------------------------------------------------------------------

# while read linha; do
#     echo "Um para ${linha:-você}, um para mim."
#     tem_linhas=
# done
# printf '%b' "${tem_linhas-Oba, é tudo meu!\n}"

# -----------------------------------------------------------------------------

if [[ -t 0 ]]; then
    printf 'Um para %s, um para mim.\n' "${@:-você}"
else
    while read linha; do
        echo "Um para ${linha:-você}, um para mim."
        tem_linhas=
    done
    printf '%b' "${tem_linhas-Oba, é tudo meu!\n}"
fi





